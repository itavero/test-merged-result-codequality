# Code Quality and Metric Reports combined with Merged Result Pipelines

During development, we noticed changes in code quality and metrics being shown in Merge Requests that only changed a
Markdown document (which is not checked by CI, so should not influence code quality or metrics).

In this case the feature branch, was branched off from the main branch, and the main branch changed after that.
Our thinking is that perhaps the code quality and metrics are being compared to the original branching point,
rather than the latest pipeline run on the main branch which is being integrated in case of a Merged Result Pipeline.

In this repository, I try to reproduce this situation.