#!/usr/bin/env bash

# Combine all the text files in metrics folder into a single file named metrics_report.txt with a new line in between
for f in metrics/*.txt; do cat "${f}"; echo; done > metrics_report.txt

# Combine all JSON files in cq into a single json file named code_quality.json, using jq
jq -s 'add' cq/*.json > code_quality.json